FROM centos:7

RUN yum install python3 python3-pip -y
COPY requirements.txt requirements.txt
RUN pip3 install -r /tmp/requirements.txt
COPY python-api.py python-api.py
EXPOSE 5290
CMD ["python3", "python-api.py"]
